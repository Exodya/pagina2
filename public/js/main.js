$(document).ready(function () {

  $("#arrow").on('click', function (event) {


    if (this.hash !== "") {

      event.preventDefault();


      var hash = this.hash;

      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 1100, function () {


        window.location.hash = hash;
      });
    }
  });
});

function open_button() {

  document.getElementById("menu").style.msTransform = "translateX(0rem)";
  
  document.getElementById("menu").style.transform = "translateX(0rem)";

  var x = document.getElementById("button");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}


function close_button() {
 
  document.getElementById("menu").style.msTransform = "translateX(16rem)";
  
  document.getElementById("menu").style.transform = "translateX(16rem)";

  var x = document.getElementById("button");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}



